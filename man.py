#!/usr/bin/env python3

import argparse
import os
import sys


def parse_args(arguments):
    parser = argparse.ArgumentParser(
        prog="man.py",
        description="Helper script to simplify the docker workflow."
    )

    subparsers = parser.add_subparsers(dest="command")

    run_parser = subparsers.add_parser("start", help="Start the containers Use"
                                                     '"man.py -d start" to run them in the backround.')
    run_parser.add_argument("-d", "--deamon-mode", help="Run the containers in the background", action="store_true")
    run_parser.add_argument("-b", "--build", help="Rebuild the image before running", action="store_true")

    subparsers.add_parser("stop", help="Stop the running container.")
    subparsers.add_parser("restart", help="Restart the running container.")

    subparsers.add_parser(
        "shell", help=(
            "Start an interactive shell to access the inside of the "
            "container. This only works if the server's container has "
            'already been started using "man.py start".'
        ),
    )

    django_parser = subparsers.add_parser("django", help='Run a manage.py command, e.g.: "./man.py django migrate"')
    django_parser.add_argument("action", help="The django manage.py command to execute", nargs=argparse.REMAINDER)

    exec_parser = subparsers.add_parser("exec", help="Run command in the container.")
    exec_parser.add_argument("action", help="The command to run", nargs=argparse.REMAINDER)

    args = parser.parse_args(arguments)

    if not args.command:
        parser.print_help()
        exit()

    return args


class Interpreter:
    @classmethod
    def interprete(cls, args: argparse.Namespace):
        command_name = args.command.replace("-", "_")

        if not hasattr(cls, command_name):
            raise NotImplementedError(f'The command "{args.command}" has not been implemented yet.')

        getattr(cls, command_name)(args)

    @classmethod
    def start(cls, args: argparse.Namespace):
        command = ["docker", "compose", "up"]
        if args.deamon_mode:
            command.append("-d")
        if args.build:
            command.append("--build")
        os.system(" ".join(command))

    @classmethod
    def stop(cls, args: argparse.Namespace):
        command = ["docker", "compose", "down"]
        os.system(" ".join(command))

    @classmethod
    def restart(cls, args: argparse.Namespace):
        command = ["docker", "compose", "restart"]
        os.system(" ".join(command))

    @classmethod
    def shell(cls, args: argparse.Namespace):
        command = [
            "docker",
            "compose",
            "exec",
            "web",
            "bash",
        ]
        os.system(" ".join(command))

    @classmethod
    def exec(cls, args: argparse.Namespace):
        command = [
            "docker",
            "compose",
            "exec",
            "web",
            " ".join(args.action),
        ]
        os.system(" ".join(command))

    @classmethod
    def django(cls, args: argparse.Namespace):
        command = [
            "docker",
            "compose",
            "exec",
            "web",
            "python manage.py",
            " ".join(args.action)
        ]
        os.system(" ".join(command))


if __name__ == "__main__":
    arguments = parse_args(sys.argv[1:])
    Interpreter.interprete(arguments)
