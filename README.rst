fidel
=====

simple RSS scraper application which saves RSS feeds to a database and lets a user view and manage feeds they’ve added to the system through an API

.. image:: https://img.shields.io/badge/based%20on-Django%20Naqsh-0952D5.svg
     :target: https://github.com/mazdakb/django-naqsh/
     :alt: Built with Django Naqsh


:License: GPLv3


logo from: https://gitlab.com/spacecowboy/Feeder

Run locally
-----------

for running locally


clone the repo

    git clone https://gitlab.com/abtinmo/fidel/ && cd fidel


build and run the project.

    python3 man.py start


Code quality
-------

check code style:

    pip install tox && tox

run tests:

    python3 man.py django test



API Documents
-------

open this address with your favorite browser:

    http://localhost:8000/swagger