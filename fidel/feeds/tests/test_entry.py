from django.contrib.auth.models import User
from django.utils import timezone
from rest_framework.test import APITransactionTestCase

from fidel.feeds.models import Entry, Feed


class EntryAPITest(APITransactionTestCase):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create(
            username="string",
            password="string",
        )
        self.feed = Feed.objects.create(
            url="https://stallman.org/rss/rss.xml",
            is_valid=True,
        )
        self.feed.users.add(self.user)
        self.client.force_authenticate(user=self.user)
        self.entry = Entry.objects.create(
            id="https://fakeurl.com",
            feed=self.feed,
            title="test_feed",
            summary="simple data",
            author="author",
            published_parsed=timezone.now(),
        )

    def test_list(self):
        # get empty list
        response = self.client.get(
            "/api/v1/feeds/entries/", format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("entries", response.data)
        self.assertIn("next", response.data)
        self.assertIn("previous", response.data)
        self.assertIn("count", response.data)
        self.assertIsInstance(response.data["entries"], list)
        self.assertEqual(len(response.data["entries"]), 1)
        entry = response.data["entries"][0]
        self.assertIn("id", entry)
        self.assertIn("date", entry)
        self.assertIn("title", entry)
