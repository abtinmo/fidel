from unittest.mock import patch

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase

from fidel.feeds.models import Feed


class FeedAPITestCase(APITestCase):

    def setUp(self):
        super().setUp()
        self.user = User.objects.create(
            username="string",
            password="string",
        )
        self.client.force_authenticate(user=self.user)
        self.feed = Feed.objects.create(
            url="https://stallman.org/rss/rss.xml",
        )
        self.feed.users.add(self.user)

    def test_list(self):
        response = self.client.get(
            "/api/v1/feeds/", format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("feeds", response.data)
        self.assertIn("next", response.data)
        self.assertIn("previous", response.data)
        self.assertIn("count", response.data)
        self.assertIsInstance(response.data["feeds"], list)
        self.assertEqual(len(response.data["feeds"]), 1)
        feed = response.data["feeds"][0]
        self.assertIn("id", feed)
        self.assertIn("title", feed)

    @patch("fidel.feeds.tasks.update_feed.delay")
    def test_create_new_feed(self, mock_object):
        data = {
          "url": "https://www.djangoproject.com/rss/weblog/",
        }
        self.assertEqual(Feed.objects.count(), 1)
        response = self.client.post(
            "/api/v1/feeds/", data=data, format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        mock_object.assert_called()
        self.assertEqual(Feed.objects.count(), 2)

    @patch("fidel.feeds.tasks.update_feed.delay")
    def test_create_existing_feed(self, mock_object):
        data = {
          "url": self.feed.url,
        }
        self.assertEqual(Feed.objects.count(), 1)
        response = self.client.post(
            "/api/v1/feeds/", data=data, format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        mock_object.assert_not_called()
        self.assertEqual(Feed.objects.count(), 1)

    def test_unsubscribe_from_feed(self):
        self.assertEqual(Feed.objects.count(), 1)
        self.assertEqual(
            Feed.objects.filter(users=self.user).count(),
            1,
        )
        response = self.client.delete(f"/api/v1/feeds/{self.feed.id}/")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Feed.objects.count(), 1)
        self.assertEqual(
            Feed.objects.filter(users=self.user).count(),
            0,
        )
