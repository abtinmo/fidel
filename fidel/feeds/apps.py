from django.apps import AppConfig


class FeedsAppConfig(AppConfig):
    name = "fidel.feeds"
    verbose_name = "Feeds"
