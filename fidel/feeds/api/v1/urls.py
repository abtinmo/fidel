from django.urls import path
from rest_framework import routers

from .views import EntryViewSet, FeedViewSet


router = routers.SimpleRouter()

urlpatterns = [
    path(
        "",
        FeedViewSet.as_view(
            {"get": "list", "post": "create"},
        ),
        name="team_list_create",
    ),
    path(
        "<int:pk>/", FeedViewSet.as_view(
            {"put": "update", "delete": "destroy"},
        ),
        name="team_get_update",
    ),
]

router.register(r"entries", EntryViewSet)

urlpatterns += router.urls
