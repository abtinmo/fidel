from rest_framework import serializers

from fidel.feeds.models import Entry, Feed
from fidel.feeds.tasks import update_feed


class CreateFeedSerializer(serializers.ModelSerializer):
    # override url field to disable unique check. as it handled in create method
    url = serializers.URLField()

    class Meta:
        model = Feed
        fields = ["id", "url"]

    def create(self, validated_data):
        """Subscribe user to the feed."""
        feed, created = Feed.objects.get_or_create(url=validated_data["url"])
        if created:
            update_feed.delay(url=feed.url, feed_id=feed.id)
        user = self.context["request"].user
        feed.users.add(user)
        return feed


class ListFeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feed
        fields = ["id", "title"]


class ListEntrySerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(source="published_parsed")

    class Meta:
        model = Entry
        fields = ["id", "date", "title"]


class DetailEntrySerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(source="published_parsed")

    class Meta:
        model = Entry
        fields = "__all__"
