from django_filters import rest_framework as filters
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from fidel.feeds.models import Entry, Feed
from utils.pagination_class import EntryPagination, FeedPagination

from .serializers import CreateFeedSerializer, DetailEntrySerializer, ListEntrySerializer, ListFeedSerializer


class FeedViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Feed.objects.all()
    pagination_class = FeedPagination

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Feed.objects.none()
        return super().get_queryset().filter()

    def get_serializer_class(self):
        if self.action == "create":
            return CreateFeedSerializer
        return ListFeedSerializer

    def perform_destroy(self, instance: Feed):
        instance.users.remove(self.request.user)


class EntryViewSet(ReadOnlyModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Entry.objects.all()
    pagination_class = EntryPagination
    filter_backends = [filters.DjangoFilterBackend]
    filterset_fields = ["feed"]

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Entry.objects.none()
        return super().get_queryset().filter(feed__users=self.request.user)

    def get_serializer_class(self):
        if self.action == "list":
            return ListEntrySerializer
        return DetailEntrySerializer
