import logging
import ssl

from datetime import datetime
from time import mktime

import feedparser

from config import celery_app
from fidel.feeds.models import Entry, Feed

from .serializers import FeedUpdateSerializer, NewEntrySerializer


ssl._create_default_https_context = ssl._create_unverified_context


@celery_app.task
def update_feed(url, feed_id):
    """
    gets url and id of feed and updates title and image
    """
    feed_data = feedparser.parse(url)
    feed = Feed.objects.get(id=feed_id)
    serializer = FeedUpdateSerializer(instance=feed, data=feed_data)
    if not serializer.is_valid():
        logging.error("Invalid feed received.", extra={"errors": serializer.errors})
        feed.is_valid = False
        feed.save()
        return
    serializer.save()


@celery_app.task
def sync_entries_periodically():
    """
    :return: None
    gets feed_id and syncs all entries with upstream,
    """
    feeds = Feed.objects.filter(is_valid=True).values("id", "url")
    for feed in feeds:
        fetch_feed_entries.delay(feed_id=feed["id"], feed_url=feed["url"])


@celery_app.task
def fetch_feed_entries(feed_id: str, feed_url: str) -> None:
    feed_data = feedparser.parse(feed_url)
    entries = []
    for entry in feed_data["entries"]:
        entry["published_parsed"] = datetime.fromtimestamp(
            mktime(entry["published_parsed"]),
        )
        entry["feed_id"] = feed_id
        serializer = NewEntrySerializer(data=entry)
        if serializer.is_valid():
            logging.error("Invalid entry.", extra={"errors": serializer.errors})
            entries.append(serializer.save(feed_id=feed_id))
    Entry.objects.bulk_create(entries, ignore_conflicts=True)
