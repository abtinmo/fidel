from rest_framework import serializers

from fidel.feeds.models import Entry, Feed


class FeedUpdateSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source="feed.title")
    image = serializers.URLField(
        source="feed.image.href", allow_null=True, allow_blank=True,
    )

    class Meta:
        model = Feed
        fields = ["title", "image"]


class NewEntrySerializer(serializers.ModelSerializer):
    feed_id = serializers.IntegerField()

    def save(self, *args, **kwargs):

        return Entry(**self.validated_data)

    class Meta:
        model = Entry
        exclude = ["feed"]
