from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _


class Feed(models.Model):
    title = models.CharField(_("Title"), max_length=1023)
    url = models.URLField(_("URL"), unique=True)
    users = models.ManyToManyField(
        User, verbose_name=_("User"), related_name="feeds",
    )
    image = models.URLField(
        _("Image"), blank=True, null=True,
        help_text=_("Logo or image of Feed if Feed has any"),
    )
    is_valid = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["title"]


class Entry(models.Model):
    id = models.URLField(
        _("UID"), primary_key=True, blank=True,
        help_text=_("Unique id of entry"),
    )
    feed = models.ForeignKey(
        Feed, verbose_name=_("Feed"), related_name="entries",
        on_delete=models.CASCADE, db_index=True,
    )
    title = models.CharField(_("Title"), max_length=255)
    summary = models.TextField(_("Summary"))
    author = models.CharField(_("Author"), max_length=1023, blank=True)
    published_parsed = models.DateTimeField(
        _("Date"),
        help_text=_("Publish date of entry"),
    )

    class Meta:
        # Display most recent entries first
        ordering = ["-published_parsed"]

    def __str__(self):
        return self.title
