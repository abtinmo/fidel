
from django.contrib import admin
from django.utils.html import format_html
from rangefilter.filters import DateRangeFilter

from .models import Entry, Feed


class FeedAdmin(admin.ModelAdmin):
    list_display = ["title", "thumbnail_image", "is_valid"]
    list_filter = ["is_valid"]
    search_fields = ["title"]

    @staticmethod
    def thumbnail_image(obj):
        return format_html(
            f'<img src="{obj.image}" style="width: 50px;"/>',
        ) if obj.image else ""


class EntryAdmin(admin.ModelAdmin):
    list_display = ["feed", "title", "author", "published_parsed"]
    list_filter = [
        "feed", "author", ("published_parsed", DateRangeFilter),
    ]


admin.site.register(Feed, FeedAdmin)
admin.site.register(Entry, EntryAdmin)
