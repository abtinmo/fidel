from django.contrib import admin
from rangefilter.filters import DateRangeFilter

from .models import Notification


class NotificationAdmin(admin.ModelAdmin):
    list_filter = ["user", "read", ("date", DateRangeFilter)]
    list_display = ["user", "read", "date"]


admin.site.register(Notification, NotificationAdmin)
