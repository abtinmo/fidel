from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from rest_framework.views import APIView

from .serializers import AccountRegisterSerializer


class AccountRegisterView(APIView):
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        request_body=AccountRegisterSerializer,
        responses={HTTP_201_CREATED: None},
    )
    def post(self, request):
        """
        Takes username and password, and creates new user
        """
        serializer = AccountRegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(is_active=True)
        return Response(status=HTTP_201_CREATED)
