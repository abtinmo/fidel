from django.apps import AppConfig


class AccountsAppConfig(AppConfig):
    name = "fidel.accounts"
    verbose_name = "Accounts"
