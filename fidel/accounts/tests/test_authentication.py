from rest_framework.test import APITestCase


class AuthTest(APITestCase):
    def test_register(self):
        # test register successfully
        data = {
            "username": "user",
            "password": "password",
        }
        response = self.client.post(
            "/api/v1/accounts/register/", data=data, format="json",
        )
        self.assertEqual(response.status_code, 201)
        # test register with existing user
        response = self.client.post(
            "/api/v1/accounts/register/", data=data, format="json",
        )
        self.assertEqual(response.status_code, 400)
        # test register with bad password
        data["password"] = ""
        response = self.client.post(
            "/api/v1/accounts/register/", data=data, format="json",
        )
        self.assertEqual(response.status_code, 400)
        # test register without username in body
        data.pop("username")
        data["password"] = "password"
        response = self.client.post(
            "/api/v1/accounts/register/", data=data, format="json",
        )
        self.assertEqual(response.status_code, 400)

    def test_login(self):
        # test login successfully
        data = {
            "username": "user",
            "password": "password",
        }
        self.client.post(
            "/api/v1/accounts/register/", data=data, format="json",
        )
        data = {
            "username": "user",
            "password": "password",
        }
        response = self.client.post(
            "/api/v1/accounts/login/", data=data, format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("refresh", response.data)
        self.assertIn("access", response.data)
        # test login with not exiting user
        data["username"] = "Sense"
        response = self.client.post(
            "/api/v1/accounts/login/", data=data, format="json",
        )
        self.assertEqual(response.status_code, 401)

    def test_refresh(self):
        # test refreshing access token successfully
        data = {
            "username": "user",
            "password": "password",
        }
        self.client.post(
            "/api/v1/accounts/register/", data=data, format="json",
        )
        data = {
            "username": "user",
            "password": "password",
        }
        response = self.client.post(
            "/api/v1/accounts/login/", data=data, format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("refresh", response.data)
        self.assertIn("access", response.data)
        refresh_token = response.data["refresh"]
        access_token = response.data["access"]
        data = {
            "refresh": refresh_token,
        }
        response = self.client.post(
            "/api/v1/accounts/refresh/", data=data, format="json",
        )
        self.assertEqual(response.status_code, 200)
        # test refreshing with wrong token
        data["refresh"] = access_token
        response = self.client.post(
            "/api/v1/accounts/refresh/", data=data, format="json",
        )
        self.assertEqual(response.status_code, 401)
        # test refreshing with empty body
        response = self.client.post(
            "/api/v1/accounts/refresh/", data={}, format="json",
        )
        self.assertEqual(response.status_code, 400)
