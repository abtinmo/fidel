from rest_framework import pagination
from rest_framework.response import Response


class FeedPagination(pagination.PageNumberPagination):
    page_size_query_param = "page_size"
    max_page_size = 400
    page_size = 50

    def get_paginated_response(self, data):
        return Response(
            {
                "next": self.get_next_link(),
                "previous": self.get_previous_link(),
                "count": self.page.paginator.count,
                "feeds": data,
            },
        )


class EntryPagination(pagination.PageNumberPagination):
    page_size_query_param = "page_size"
    max_page_size = 400
    page_size = 50

    def get_paginated_response(self, data):
        return Response(
            {
                "next": self.get_next_link(),
                "previous": self.get_previous_link(),
                "count": self.page.paginator.count,
                "entries": data,
            },
        )
