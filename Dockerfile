FROM python:3.11-slim-buster

# Set work directory
WORKDIR /code

ENV PYTHONUNBUFFERED 1

COPY requirements/base.txt /code/
COPY requirements/local.txt /code/
RUN pip install -r local.txt
COPY . /code/
