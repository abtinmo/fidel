from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view


urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/v1/", include([
        path(
            "accounts/",
            include("fidel.accounts.api.v1.urls"),
            name="accounts",
        ),
        path(
            "feeds/",
            include("fidel.feeds.api.v1.urls"),
            name="feeds",
        ),
    ])),
]

if settings.DEBUG:
    schema_view = get_schema_view(
        openapi.Info(
            title="Fidel API",
            default_version="v1",
            contact=openapi.Contact(email="abtin@riseup.net"),
            license=openapi.License(
                name="GENERAL PUBLIC LICENSE Version 3",
            ),
        ),
        public=True,
    )
    urlpatterns += [
        path(
            "swagger/", schema_view.with_ui("swagger", cache_timeout=0),
            name="schema-swagger-ui",
        ),
    ]
